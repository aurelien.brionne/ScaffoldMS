<img src="./inst/extdata/univ.png" align="right"/>
<img src="./inst/extdata/boa.png" align="right"/>
<img src="./inst/extdata/inra.png" align="right"/>

# ScaffoldMS R package

Enhanced analysis of protein mass spectrometry results provided by Scaffold Proteome software &copy;.

## Installation

```r
## using install_gitlab
remotes::install_gitlab(
    "aurelien.brionne/ScaffoldMS",
    host = "forgemia.inra.fr",
    build_opts = c("--no-resave-data", "--no-manual")
)

## alternative (from local directory)
    # clone package (from prompt)
    git clone https://forgemia.inra.fr/aurelien.brionne/ScaffoldMS.git

    # build package (from R console) 
    devtools::build("ScaffoldMS")

    # install package (from R console)
    install.packages("ScaffoldMS_1.2.1.tar.gz", repos = NULL, type = "source")
````

## Quick overview

1. Annotate proteins: `ScaffoldMS::annotate`.

* Identification of the species of origin from Genbank data (taxon, scientific name, common name).
* Gene annotation summary.
* construction of the peptide signature ("pattern") allowing the identification of proteins and their grouping using
[Blast +](http://www.ncbi.nlm.nih.gov/books/NBK1763/) suite and [clustal omega](http://www.clustal.org/omega).

![](./inst/extdata/frag.png)

2. perform Linear model statistics from Scaffold Proteome software &copy; quantification files (current view): `ScaffoldMS::quantify`.

3. Merge pattern alignments (clustalo and blast) results: `ScaffoldMS::merge_alignments`.

4. Display proteins species count overview: `ScaffoldMS::species_count`.

![](./inst/extdata/species.png)

5. Proteins abundance profiling: `ScaffoldMS::HCL`.

![](./inst/extdata/heatmap.png)

![](./inst/extdata/profils.png)
</br>
</br>
</br>
</br>

6. Proteins abundance plot: `ScaffoldMS::abundance_plot`.

![](./inst/extdata/emPAIvsNSAF.png)


